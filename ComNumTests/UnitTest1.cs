﻿using System;
using ComplexNumbers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ComNumTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_Addition()
        {
            //параметры
            var first = new Complex(5, 7);
            var second = new Complex(2, 3);
            var expected = new Complex(7, 10);

            //действия
            var result = Complex.Addition(first,second);

            Assert.AreEqual(expected.Real, result.Real);
            Assert.AreEqual(expected.Imaginary, result.Imaginary);
        }

        [TestMethod]
        public void Test_Substraction()
        {
            //параметры
            var first = new Complex(5, 7);
            var second = new Complex(2, 3);
            var expected = new Complex(3, 4);

            //действия
            var result = Complex.Substraction(first, second);

            Assert.AreEqual(expected.Real, result.Real);
            Assert.AreEqual(expected.Imaginary, result.Imaginary);
        }

        [TestMethod]
        public void Test_Multiplication()
        {
            //параметры
            var first = new Complex(5, 7);
            var second = new Complex(2, 3);
            var expected = new Complex(-11, 29);

            //действия
            var result = Complex.Multiplication(first, second);

            Assert.AreEqual(expected.Real, result.Real);
            Assert.AreEqual(expected.Imaginary, result.Imaginary);
        }

        [TestMethod]
        public void Test_Constructor()
        { 
        //пареметры
            double expectedReal = 4;
            double expectedImaginary = 3;
            double expectedModule = 5;
            double expectedArgument = 0.013089221823;

            Complex result = new Complex(expectedReal,expectedImaginary);


            Assert.AreEqual(expectedReal, result.Real);
            Assert.AreEqual(expectedImaginary, result.Imaginary);
            Assert.AreEqual(expectedModule, result.Module);

        }

        //сравнение чисел 

        [TestMethod]
        public void Test_Comparing_Positive()
        {
            var first = new Complex(1, 2);
            var second = new Complex(1, 2);

            if (first.GetHashCode() == second.GetHashCode())
            {
                Assert.AreEqual(first, second);
            }
            else
                Assert.Fail();   
        }

        [TestMethod]
        public void Test_Comparing_Negative()
        {
            var first = new Complex(5,9);
            var second = new Complex(3,15);

            Assert.AreNotEqual(first,second);
        }


        //тестирование метода ToString

        [TestMethod]
        public void Test_ToString()
        {
            //реальная и мнимая части больше нуля

            var first = new Complex(5,3);
            var expectedFirst = "5+3i";

            var resultFirst = first.ToString();

            Assert.AreEqual(expectedFirst,resultFirst);

            //мнимая часть равна нулю

            var second = new Complex(5,0);
            var expectedSecond = "5";

            var resultSecond = second.ToString();

            Assert.AreEqual(expectedSecond, resultSecond);

            //реальная часть равна нулю

            var third = new Complex(0, 3);
            var expectedThird = "3i";

            var resultThird = third.ToString();

            Assert.AreEqual(expectedThird, resultThird);

            //реальная часть меньше нуля

            var forth = new Complex(-5, 3);
            var expectedForth = "-5+3i";

            var resultForth = forth.ToString();

            Assert.AreEqual(expectedForth, resultForth);

            //мнимая часть меньше нуля

            var fifth = new Complex(5, -3);
            var expectedFifth = "5-3i";

            var resultFifth = fifth.ToString();

            Assert.AreEqual(expectedFifth,resultFifth);

            //реальная и мнимая части меньше нуля

            var sixth = new Complex(-5,-3);
            var expectedSixth = "-5-3i";

            var resultSixth = sixth.ToString();

            Assert.AreEqual(expectedSixth,resultSixth);
        }
    }
}
