﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Complex complex1 = new Complex(5,7);
            Complex complex2 = new Complex(2,3);

            //Сложение
            Console.WriteLine("Сложение");
            Complex complex3 = Complex.Addition(complex1,complex2);
            Console.WriteLine(complex3);
            Console.WriteLine();

            //Вычитание
            Console.WriteLine("Вычитание");
            Complex complex4 = Complex.Substraction(complex1, complex2);
            Console.WriteLine(complex4);
            Console.WriteLine();

            //Умножение
            Console.WriteLine("Умножение");
            Complex complex5 = Complex.Multiplication(complex1, complex2);
            Console.WriteLine(complex5);
            Console.WriteLine();

            //сравнение двух объектов
            if (complex1.GetHashCode() == complex2.GetHashCode())
            {
              if (complex1.Equals(complex2))
              Console.WriteLine("Числа равны");

              else Console.WriteLine("Числа не равны");
            }

            else Console.WriteLine("Числа не равны");
                
        }
    }
}
