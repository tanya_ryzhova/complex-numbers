﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexNumbers
{
        public class Complex
        {
            private readonly double real; //реальная часть комплексного числа
            private readonly double imaginary; //мнимая часть комплексного числа
            private readonly double module; //модуль
            private readonly double argument; //аргумент

            public double Real 
            { 
                get{ return real;}
            }

            public double Imaginary
            {
                get { return imaginary;}
            }

            public double Module
            {
                get { return module;}
            }

            public double Argument
            {
                get { return argument;}
            }

            //public Complex()
            //    : this(0, 0)
            //{ }

            public Complex(double r = 0, double im = 0)
            {
                real = r;
                imaginary = im;
                module = Math.Abs(Math.Sqrt((real * real) + (imaginary * imaginary)));
                argument = Math.Atan(imaginary / real); 
            }

            //переопределение методов GetHashCode И AreEqual

            public override int GetHashCode()
            {
                return real.GetHashCode() ^ imaginary.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                Complex f = obj as Complex;
                if (f == null)
                    return false;

                return f.real == real && f.imaginary == imaginary;
            }


            //Сложение
            public static Complex Addition(Complex x, Complex y)
            {
                return new Complex(x.real + y.real, x.imaginary + y.imaginary);
            }

            //Вычитание
            public static Complex Substraction(Complex x, Complex y)
            {
                return new Complex(x.real - y.real, x.imaginary - y.imaginary);
            }

            //Умножение
            public static Complex Multiplication(Complex x, Complex y)
            {
                return new Complex((x.real * y.real) - (x.imaginary * y.imaginary), (x.imaginary * y.real) + (x.real * y.imaginary));
            }

            ////Модуль
            //public static double Module(Complex x)
            //{
            //   double z = Math.Sqrt((x.real * x.real) + (x.imaginary * x.imaginary));
            //    return Math.Abs (z);
            //}

            ////Аргумент
            //public static double Argument(Complex x)
            //{
            //    double f = x.imaginary / x.real;
            //    return Math.Tan(f);
            //}



            //Переопределение 
            public override string ToString()
            {
                if (imaginary > 0 & real > 0)
                {
                    return string.Format("{0}+{1}i", real, imaginary);
                }
                if (Math.Abs(imaginary) < 0.001)
                {
                    return string.Format("{0}", real);
                }
                if (imaginary < 0)
                {
                    return string.Format("{0}{1}i", real, imaginary);
                }
                if (Math.Abs(real) < 0.001)
                {
                    return string.Format("{0}i", imaginary);
                }
                if (real < 0)
                {
                    return string.Format("{0}+{1}i", real, imaginary);
                }
                else
                {
                    return string.Format("-({0}+{1}i)", real, imaginary);
                }

            }
        }
    }
